/*
 * Имя класса Converter. Конвертер договора в акт.
 *
 * Конвертирует поля с договора в поля акта. Урок 8ый ДЗ_8_4
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson8.Dogovor;


public class Converter {

    static Act converter(Dogovor dogovor) {
        return new Act(dogovor.getNumber(), dogovor.getDate(), dogovor.getDocTmc());
    }

    public static void main(String[] args) {
    }
}
