package ru.khalitov.lesson20;

//import com.fasterxml.jackson.databind.DeserializationFeature;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class JSON {
    public static void main(String[] args) {
        try {
            URL json = new URL("https://dog.ceo/api/breeds/image/random");
            try (InputStream is = json.openStream();
                 Reader reader = new InputStreamReader(is);
                 BufferedReader br = new BufferedReader(reader)) {
                String line;
                while ((line = br.readLine()) != null) {
//                    ObjectMapper mapper = new ObjectMapper();
//                    mapper.enable(SerializationFeature.INDENT_OUTPUT);
//                    mapper.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
//                    Dog1 dog = mapper.readValue(line, Dog1.class);
//                    System.out.println(dog.toString());
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
