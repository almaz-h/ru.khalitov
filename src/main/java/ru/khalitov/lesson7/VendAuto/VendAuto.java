package ru.khalitov.lesson7.VendAuto;


import java.util.logging.Logger;

class VendAuto {

    final static Logger logger = Logger.getLogger(VendAuto.class.getName());
    Juces[] menu = {Juces.COCACOLA, Juces.PEPSI, Juces.SPRITE};


    private int money;

    void getMenu() {
        logger.info("Тут вызывается меню напитков");

        for (int i = 0; i < menu.length; i++)
            System.out.println("Номер напитка: " + (i + 1) + " Название напика: " + menu[i].getName() +
                    " Цена напитка: " + menu[i].getPrice());
    }

    void getDrink(int drink) {
        logger.info("Здесь происходит выбор напитков");
        if (drink > 3) {
            logger.warning("Если вы выберите напиток, которого нет программа завершится!");
            System.out.println("Такого напитка нет");
        } else if (menu[drink - 1].getPrice() == money) {
            System.out.println("Вы купили: " + menu[drink - 1].getName());
        } else
            System.out.println("Неверная сумма");
    }


    void putMoney(int money) {
        logger.info("Здесь вы вносите деньги за напиток");
        this.money += money;
        System.out.printf("Вы внесли %d рублей\n", money);

    }
}
