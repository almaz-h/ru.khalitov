package ru.khalitov.lesson29;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class MapRelease {

    public static void main(String[] args) {

        Map<String, Person> book = createMap();
        showStore("Создали мапу", book);
        removeTheDuplicates(book);
        showStore("Удалили дубли", book);
    }

    private static void showStore(String title, Map<String, Person> book) {
        System.out.println(title);
        for (Map.Entry<String, Person> entry : book.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }

    private static Map<String, Person> createMap() {
        Map<String, Person> book = new HashMap<>();
        Person person1 = new Person("Аня", "Петрова", 29);
        Person person2 = new Person("Катя", "Сидорова", 34);
        Person person3 = new Person("Ксюша", "Тихонова", 34);
        Person person4 = new Person("Павел", "Петров", 35);
        book.put("Key1", person1);
        book.put("Key2", person1);
        book.put("Key3", person2);
        book.put("Key4", person3);
        book.put("Key5", person4);
        book.put("Key6", person4);
        return book;
    }

    private static void removeTheDuplicates(Map<String, Person> book) {
        /*
        Реализации функции через метод удаления оригиналов с дубликатами
         */
//        HashMap<String, Person> map = new HashMap<>(book);
//        for (Map.Entry<String, Person> iter : map.entrySet()) {
//            int frequency = Collections.frequency(map.values(), iter.getValue());  // применяем метод Collections.frequency, находит повторения в виде числа > 1
//            if (frequency > 1) {
//                removeItemFromMapByValue(book, iter.getValue());
//            }
//        }

        Iterator<Map.Entry<String, Person>> iter = book.entrySet().iterator();
        HashSet<Person> valueSet = new HashSet<>();
        while (iter.hasNext()) {
            Map.Entry<String, Person> next = iter.next();
            if (valueSet.contains(next.getValue())) {
                iter.remove();
            } else {
                valueSet.add(next.getValue());
            }
        }
    }

    /*
    Метод удаляет дубликаты вместе с оригиналами записей из Мапы
     */
    private static void removeItemFromMapByValue(Map<String, Person> book, Person value) {
        Map<String, Person> copy = new HashMap<>(book);
        for (Map.Entry<String, Person> pair : copy.entrySet()) {
            if (pair.getValue().equals(value))
                book.remove(pair.getKey());
        }
    }
}
