/*
 * Имя класса Task2.Программа, считает зарплату на "руки"
 *
 * Версия три, ввод с консоли , Урок 3ий, ДЗ_3_2
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson3;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Моя зарплата до вычета НДФЛ");
        int zarplata = in.nextInt();
        double sum = zarplata / 1.13;
        System.out.printf("Зарплатa на руки: %.2f", sum);

    }
}