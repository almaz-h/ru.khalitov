package ru.khalitov.lesson31;

public class Palindrome {
    public static void main(String[] args) {

        String line = "мадам";
        String line2 = "шалаш";

        System.out.println("Строка палиндром? " + isPalindrome(line));
        System.out.println("Срока палиндром? " + isPalindrome2(line2));
    }

    private static Boolean isPalindrome(String s) {
        return s.equals(new StringBuilder(s).reverse().toString());
    }

    private static Boolean isPalindrome2(String s) {
        for (int i = 0; i < s.length() / 2; ++i) {
            if (s.charAt(i) != s.charAt(s.length() - i - 1)) {
                return false;
            }
        }
        return true;
    }
}

