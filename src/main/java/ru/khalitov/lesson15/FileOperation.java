package ru.khalitov.lesson15;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileOperation {
    public static void main(String[] args) {

        try {
            Path file = Files.createFile(Paths.get("C:\\Users\\User\\IdeaProjects\\HomeWorks" +
                    "\\src\\main\\java\\ru\\khalitov\\lesson14\\file.txt"));
            System.out.println("Создали файл " + Files.exists(Paths.get("C:\\Users\\User\\IdeaProjects\\HomeWorks" +
                    "\\src\\main\\java\\ru\\khalitov\\lesson14\\file.txt")));
            System.out.println("Имя созданного файла: " + file.getFileName());

            try {
                Files.createDirectory(Paths.get("C:\\Users\\User\\IdeaProjects\\HomeWorks" +
                        "\\src\\main\\java\\ru\\khalitov\\lesson14\\directory"));
                System.out.println("Создали директорию: " + Files.exists(Paths.get("C:\\Users\\User\\IdeaProjects\\HomeWorks" +
                        "\\src\\main\\java\\ru\\khalitov\\lesson14\\directory")));
            } catch (IOException ex) {
                System.out.println("Исключение, директория уже есть: " + ex.getMessage());
            }

            file = getPathCopy(file);
            getDelete(file);

        } catch (IOException e) {
            System.out.println("Исключение, файл уже создан: " + e.getMessage());
        } finally {
            System.out.println("Программа завершила выполнение");
        }
    }

    private static void getDelete(Path file) throws IOException {
        Files.delete(file);
        System.out.println("Файл есть в директории?" + Files.exists(Paths.get("C:\\Users\\User\\IdeaProjects" +
                "\\HomeWorks" +
                "\\src\\main\\java\\ru\\khalitov\\lesson14\\directory\\file.txt")));
    }

    private static Path getPathCopy(Path file) throws IOException {
        file = Files.copy(file, Paths.get("C:\\Users\\User\\IdeaProjects\\HomeWorks" +
                "\\src\\main\\java\\ru\\khalitov\\lesson14\\directory\\file.txt"));
        System.out.println("Скопировали файл? " + Files.exists(Paths.get("C:\\Users\\User\\IdeaProjects\\HomeWorks" +
                "\\src\\main\\java\\ru\\khalitov\\lesson14\\directory\\file.txt")));
        return file;
    }
}
