/*
 * Имя класса Act. Конвертер договора в акт.
 *
 * Данные по акту(поля). Урок 8ый ДЗ_8_4
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson8.Dogovor;

class Act {
    private int number;
    private int date;
    private String[] actTmc;

    Act(int number, int date, String... actTmc) {
        this.number = number;
        this.date = date;
        this.actTmc = actTmc;
    }

    public String[] getActTmc() {
        return actTmc;
    }

    public int getNumber() {
        return number;
    }

    public int getDate() {
        return date;
    }


    @Override
    public String toString() {
        return "Act{" +
                "number=" + number +
                ", date=" + date +
                ", name='" + actTmc + '\'' +
                '}';
    }
}