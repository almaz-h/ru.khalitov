/*
 * Имя класса Task4. Арифметиеская прогрессия
 *
 * Урок 4ый, Д34_4
 *
 * Выполнил Алмаз Халитов
 */

package ru.khalitov.lesson4;

import ru.khalitov.tools.ConsoleScanner;

public class Task4_Progressia {
    public static void main(String[] args) {
        int a = ConsoleScanner.NextFromConsole(); // первое значение арифметической прогрессии
        int b = ConsoleScanner.NextFromConsole(); // шаг арифметической прогрессии
        int N = ConsoleScanner.NextFromConsole(); // кол-во элементов прогрессии
        for (int i = 0; i < N; i++) {
            System.out.println(a);
            a = a + b;
        }
    }
}
