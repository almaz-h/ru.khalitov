/*
 * Имя класса Task3. Программа, конвертирует секунды в минуты
 *
 * Версия три, ввод с консоли , Урок 3ий, ДЗ_3_2
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson3;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите кол-во секуд");
        int sec = in.nextInt();
        float hour = sec/3600f;
        System.out.printf("В %d секундах, %.2f часа",sec,hour);
    }
}
