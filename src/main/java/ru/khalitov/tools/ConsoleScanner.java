package ru.khalitov.tools;

import java.util.Scanner;

public class ConsoleScanner {
    public static int NextFromConsole() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Здравствуйте, введите пожалуйста число");
        while (!sc.hasNextInt()) {
            System.out.println("Это не число , введите число!");
            sc.next();
        }
        return sc.nextInt();
    }
}


