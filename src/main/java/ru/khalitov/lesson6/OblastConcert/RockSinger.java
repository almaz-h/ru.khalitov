package ru.khalitov.lesson6.OblastConcert;

class RockSinger extends Singer {

    RockSinger(String name, int age) {
        super(name, age);
    }

    void singRock() {
        System.out.println("арр,уу-уу\n");
    }

    @Override
    void sing() {
        System.out.println(toString() + " и я пою рок!");
    }

    @Override
    public String toString() {
        return super.toString();
    }
}