package ru.khalitov.lesson17.HomeLibrary;

import java.io.Serializable;

public class Books implements Serializable {
    final static long serialVersionUID = 1l;

    String author;
    String bookName;
    int date;

    public Books(String author, String bookName, int date) {
        this.author = author;
        this.bookName = bookName;
        this.date = date;
    }

    public Books() {

    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Книга: " +
                "Автор книги: " + author +
                ", название: " + bookName +
                ", год выпуска книги: " + date + "\n";
    }
}

