package ru.khalitov.lesson19;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Tab {
    public static void main(String[] args) {
        float count = 0;
        System.out.printf("%12s %11s %8s %10s", "Наименование", "Цена", "Кол-во", "Стоимость" + "\n");
        System.out.println("===========================================");
        try (Scanner sc = new Scanner(new File
                ("src\\main\\java\\ru\\khalitov\\lesson19\\product.txt"))) {

            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                String[] array = line.split("\\;");
                String name = array[0];

                Float amount = Float.valueOf(array[1]);
                Float price = Float.valueOf(array[2]);

                Float sum = price * amount;
                count += sum;

                System.out.printf("%-18s%6.2f X %5.3g =%8.2f \n", name, price, amount, sum);
            }
            System.out.println("===========================================");
            System.out.printf("Итого: %36.2f", count);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

