/*
 * Имя класса Task1.Программа, считает стоимость бензина
 *
 * Версия два, через аргемент, Урок 2ой, ДЗ_2_1
 *
 * Выполнил Алмаз Халитов
 */

package ru.khalitov.lesson2;

public class Task1 {
    public static void main(String[] args) {
        String unit = args[0]; // заправляем бензина 15 л
        Integer unit1 = Integer.valueOf(unit);
        int price = 42;
        int sum = unit1 * price;

        System.out.println(sum);
    }
}