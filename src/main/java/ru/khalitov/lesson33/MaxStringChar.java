package ru.khalitov.lesson33;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class MaxStringChar {
    public static void main(String[] args) {
        String str = "This is test message";
        findChar2(str);
    }

    private static void findChar2(String line) {
        Map<Character, Integer> mapChar = new HashMap<>();
        for (int i = 0; i < line.length(); i++) {
            Character ch = line.charAt(i);
            if (null != mapChar.get(ch)) {
                mapChar.put(ch, mapChar.get(ch) + 1);
            } else
                mapChar.put(ch, 1);
        }
        int maxvalue = Collections.max(mapChar.values());
        for (Map.Entry<Character, Integer> entry : mapChar.entrySet()) {
            if (entry.getValue() == maxvalue) {
                System.out.println("Символ (" + entry.getKey() + "), повторяется " + maxvalue + " раз!");
            }
        }
    }
}
