package ru.khalitov.lesson32;

class Node {
    private int data;
    Node left;
    Node right;

    Node(int item) {
        this.data = item;
        left = null;
        right = null;
    }
}
