package ru.khalitov.lesson15;

import java.io.File;
import java.io.IOException;

public class RecursFile {
    public static void main(String[] args) {
        createTestFile();
        showFile(new File("src\\main\\java\\ru\\khalitov\\lesson15\\myData"));
    }

    private static void createTestFile() {
        try {
            new File("src\\main\\java\\ru\\khalitov\\lesson15\\myData\\a\\b\\c").mkdirs();
            new File("src\\main\\java\\ru\\khalitov\\lesson15\\myData\\a\\1.txt").createNewFile();
            new File("src\\main\\java\\ru\\khalitov\\lesson15\\myData\\a\\b\\2.txt").createNewFile();
            new File("src\\main\\java\\ru\\khalitov\\lesson15\\myData\\a\\b\\c\\3.txt").createNewFile();
        } catch (IOException e) {
            System.out.println("Исключение,такой файл или каталог создан: " + e.getMessage());
        }
    }

    private static void showFile(File f) {
        File[] files = f.listFiles();
        assert files != null;
        for (File p : files) {
            if (!p.isDirectory()) {
                System.out.println("Файл: " + p.getName());
            } else {
                System.out.println("Директория: " + p.getAbsolutePath());
                try {
                    showFile(p);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
