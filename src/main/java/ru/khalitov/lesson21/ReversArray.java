/*
 * Имя класса ReversArray. Реверс массива
 *
 * Версия один. Урок 21 ДЗ_21_2
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson21;

public class ReversArray {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        System.out.println("Массив до сортировки:");
        showArray(array);
        System.out.println("Массив после сортировки:");
        swap(array);
        showArray(array);
    }

    private static void swap(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            int temp;
            temp = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = temp;
        }
    }

    private static void showArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

}
