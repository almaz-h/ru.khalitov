package ru.khalitov.lesson13;

enum Food {
    CARROT("Морковь"), APPLE("Яблоко"), PORRIDGE("Каша"), SOUP("Суп");
    String name;

    Food(String name) {
        this.name = name;
    }
}



