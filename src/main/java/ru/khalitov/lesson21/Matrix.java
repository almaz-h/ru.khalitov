/*
 * Имя класса Matrix. Сдвиг массива
 *
 * Версия один. Урок 21 ДЗ_21_1
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson21;

public class Matrix {
    public static void main(String[] args) {
        int[][] matrix = {
                {1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
                {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
        };

        System.out.println("Массив исходный:");
        showElements(matrix);
        System.out.println("Массив после сдвига:");
        toLeft(matrix);
        showElements(matrix);
    }

    private static void toLeft(int[][] matrix) {
        int temp = 0;
        int a;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 1; j < matrix[i].length; ++j) {
                a = matrix[i][j];
                matrix[i][j] = matrix[i][j - 1];
                matrix[i][j - 1] = a;
            }
            matrix[0][9] = temp;
        }

        matrix[1][9] = temp;
    }


    private static void showElements(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }
}

