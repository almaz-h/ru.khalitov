package ru.khalitov.lesson6.OblastConcert;

class Concert {
    public static void main(String[] args) {
        RapSinger rapSinger = new RapSinger("Баста", 39);
        RockSinger rockSinger = new RockSinger("Илья Лагутенко, из группы \"Мумий Троль\"", 36);

        Club club = new Club();

        club.singAtTheClub(rockSinger);
        rockSinger.singRock();

        club.singAtTheClub(rapSinger);
        rapSinger.dance();
    }
}
