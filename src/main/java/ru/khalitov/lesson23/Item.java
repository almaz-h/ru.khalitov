package ru.khalitov.lesson23;

public class Item {
    private String name;
    private int quantity;

    Item(String name, int quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getName() {
        return name;
    }

    public int addQuantity(int quantity) {
        return this.quantity += quantity;
    }

    @Override
    public String toString() {
        return
                "product='" + name + '\'' +
                        ", quantity=" + quantity +
                '}';
    }
}
