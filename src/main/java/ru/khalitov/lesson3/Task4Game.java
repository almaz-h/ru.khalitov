/*
 * Имя класса Task4Game. Программа загадывает случайно число, пользователь угадывает
 *
 * Версия три, игра. Урок 3ий, ДЗ_3_4
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson3;

import ru.khalitov.tools.ConsoleScanner;

public class Task4Game {

    public static void main(String[] args) {

        System.out.println("Попробуйте отгадать число от 1 до 100!");
        doStart();
    }

    private static void doStart() {
        int prog = (int) (Math.random() * 100) + 1;

        while (true) {
            int user = ConsoleScanner.NextFromConsole();
            if (user == 101) {
                break;
            }
            int raznica = Math.abs(prog - user);
            if (raznica == 0) {
                System.out.println("Поздравлям, вы отгадали число!");
                break;
            } else if (raznica <= 11) {
                System.out.println("Горячо!");
                System.out.println("Для выхода из прогаммы нажмите цифру 101, или продолжите отгадывать");
            } else {
                System.out.println("Холодно!");
                System.out.println("Для выхода из прогаммы нажмите цифру 101, или продолжите отгадывать");
            }
        }
    }
}


