/*
 * Имя класса Task1_voidFinal. Потренироваться с Final
 *
 * Версия один. Урок 8ый ДЗ_8_1
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson8;


class Task1_voidFinal {

    final String company = "AkBars";
    final int colleagues = 1000;
    int colleagues1 = 10;

    final void myCompany() {
        System.out.println("Компания называется " + company);
    }

    final void myColleagues() {
        System.out.println("Нет открытых вакансий для сотрудников");
    }

    void myColleagues1() {
        System.out.println("До набора сотрудников в филиале было: " + colleagues1 + " людей");
    }

    void myColleagues2() {
        colleagues1++;
    }

    void myColleagues3() {
        myColleagues2();
        System.out.printf("Теперь в компании %d людей", colleagues1);

    }

    public static void main(String[] args) {
        Task1_voidFinal TaskFinal = new Task1_voidFinal();
        TaskFinal.myCompany();
        TaskFinal.myColleagues();
        TaskFinal.myColleagues1();
        TaskFinal.myColleagues2();
        TaskFinal.myColleagues3();
    }
}
