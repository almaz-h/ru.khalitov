package ru.khalitov.lesson9.Animals;

public class Eagle extends Animal implements Fly {

    public void getName() {
        System.out.println("Eagle");
    }

    public String getFly() {
        return ("Орёл летит");
    }
}
