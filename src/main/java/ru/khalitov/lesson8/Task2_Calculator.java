/*
 * Имя класса Task2_Calculator. Калькулятор
 *
 * Версия один. Урок 8ый ДЗ_8_2
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson8;

public class Task2_Calculator {

    static double Method1(double a, double b) {
        return a * b;
    }

    static double Method2(double a, double b) {
        return a / b;
    }

    static double Method3(double a, double b) {
        return a + b;
    }

    static double Method4(double a, double b) {
        return a - b;
    }

    static double Method5(double b, double a) {
        double c = (b / a) * 100.00;
        return c;
    }

    public static void main(String[] args) {
    }
}
