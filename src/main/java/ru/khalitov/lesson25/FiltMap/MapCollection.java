/*
 * Имя класса MapCollection. Фильтр мапы
 *
 * Версия один. Урок 25 ДЗ_25_1
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson25.FiltMap;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MapCollection {
    public static void main(String[] args) {

        Map<String, String> person = createMap();
        System.out.println("Создали коллекцию, где есть одинаковые значения: \n" + createMap());
        System.out.println(isUnique(person));
        Map<String, String> person2 = createMap2();
        System.out.println("Создали коллекцию, где нет одинаковых значений: \n" + createMap2());
        System.out.println(isUnique(person2));
    }

    private static Map<String, String> createMap() {
        Map<String, String> person = new HashMap<>();
        person.put("Вася", "Иванов");
        person.put("Петр", "Иванов");
        person.put("Виктор", "Иванов");
        person.put("Сергей", "Савельев");
        person.put("Вадим", "Савельев");
        return person;
    }

    private static Map<String, String> createMap2() {
        Map<String, String> person2 = new HashMap<>();
        person2.put("Вася", "Иванов");
        person2.put("Петр", "Петров");
        person2.put("Виктор", "Сидоров");
        person2.put("Сергей", "Савельев");
        person2.put("Вадим", "Викторов");
        return person2;
    }

    private static boolean isUnique(Map<String, String> map) {

        HashMap<String, String> copy = new HashMap<>(map);
        for (Map.Entry<String, String> pair : copy.entrySet()) {
            int freqeuncy = Collections.frequency(copy.values(), pair.getValue());
            if (freqeuncy > 1) {
                System.out.println("Найдено больше 1-го одинакового значения");
                return false;
            }
        }
        System.out.println("Одинаковые значения не найдены");
        return true;
    }
}
