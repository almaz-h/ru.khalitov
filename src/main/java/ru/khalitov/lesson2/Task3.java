/*
 * Имя класса Task3. Программа, конвертирует секунды в часы
 *
 * Версия два, через аргемент. Урок 2ой ДЗ_2_3
 *
 * Выполнил Алмаз Халитов
 */

package ru.khalitov.lesson2;

public class Task3 {
    public static void main(String[] args) {
        String sec1 = args[0]; // время в секундах
        Integer sec = Integer.valueOf(sec1);
        int hour = sec / 3600; // переводим секунды в часы
        System.out.println("Часов в секундах: "+hour);

    }
}
