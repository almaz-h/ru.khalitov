package ru.khalitov.lesson9.Animals;

class Bear extends Animal implements Run, Swim {

    public void getName() {
        System.out.println("Bear");
    }

    public String getRun() {
        return ("Медведь бежит");
    }

    public String getSwim() {
        return ("Медведь плывёт");
    }
}
