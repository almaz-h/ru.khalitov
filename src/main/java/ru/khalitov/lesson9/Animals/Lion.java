package ru.khalitov.lesson9.Animals;

public class Lion extends Animal implements Run {

    public void getName() {
        System.out.println("Lion");
    }

    public String getRun() {
        return ("Лев бежит");
    }
}
