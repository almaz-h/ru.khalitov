package ru.khalitov.lesson17.HomeLibrary;


import java.io.*;
import java.util.Objects;

public class Library {
    private String library = "Library.txt";


    public static void main(String[] args) {
        Books book = new Books("А.С. Пушкин", "Руслан и Людмилла", 1820);
        Books book2 = new Books("А.Н. Толстой", "Война и Мир", 1869);
        Books book3 = new Books("Джоан Роулинг", "Гарри Поттер и Филосовский Камень", 1997);
        new Library().addInLibrary(book, book2, book3);
        new Library().readBooks();

    }


    private void addInLibrary(Books... books) {
        for (Books book : books) {
            try (FileOutputStream fos = new FileOutputStream(Library.this.library);
                 ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                oos.writeObject(books);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Добавляем в библиотеку- " + book.toString());
        }
    }

    private void readBooks() {
        Books[] books = null;
        try (FileInputStream fis = new FileInputStream(Library.this.library);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            books = (Books[]) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Считываем добавленные в библиотеку книги:");
        for (Books book : Objects.requireNonNull(books)) {
            System.out.println(book.toString());
        }
    }
}


