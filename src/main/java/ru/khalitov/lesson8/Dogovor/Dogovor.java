/*
 * Имя класса Dogovor. Конвертер договора в акт.
 *
 * Данные по договору(поля). Урок 8ый ДЗ_8_4
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson8.Dogovor;

class Dogovor {
    private int number;
    private int date;
    private String[] docTmc;

    Dogovor(int number, int date, String... docTmc) {
        this.number = number;
        this.date = date;
        this.docTmc = docTmc;
    }

    int getDate() {
        return date;
    }

    public String[] getDocTmc() {
        return docTmc;
    }

    int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return "Dogovor{" +
                "number=" + number +
                ", date=" + date +
                ", name='" + docTmc + '\'' +
                '}';
    }
}
