/*
 * Имя класса Task1.Программа, считает стоимость бензина
 *
 * Версия три, ввод с консоли , Урок 3ий, ДЗ_3_1
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson3;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Benzin();
        /*
         *данный метод мог вызваться с аналогичного по структуре класса в другом пакете
         */
    }

    private static void Benzin() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите кол-во литров бензина");
        int unit = in.nextInt();
        int price = 42;
        int sum = price * unit;
        System.out.printf("Цена бензина: %d", sum);
    }
}
