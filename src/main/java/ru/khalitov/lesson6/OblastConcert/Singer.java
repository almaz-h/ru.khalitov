package ru.khalitov.lesson6.OblastConcert;

abstract class Singer {

    private String name;
    private int age;

    Singer(String name, int age) {
        this.name = name;
        this.age = age;
    }

    abstract void sing();

    @Override
    public String toString() {
        return "Я " +
                name +
                " мне " + age +
                " лет";
    }
}
