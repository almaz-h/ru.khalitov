/*
 * Имя класса Task2. Программа, считает зарплату на "руки"
 *
 * Версия два, через аргемент. Урок 2ой, ДЗ_2_2
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson2;

public class Task2 {
    public static void main(String[] args) {
        String zp = args[0]; // зарплата к начислению 60 000
        Integer zp1 = Integer.valueOf(zp);
        double nalog = 1.13;
        double sum = zp1 / nalog;
        System.out.printf("Зарплата на руки: %.2f",sum);
    }
}
