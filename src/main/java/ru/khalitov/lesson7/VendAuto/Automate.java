package ru.khalitov.lesson7.VendAuto;
//файл лога в корневой папке урока

import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.LogManager;

public class Automate {

    public static void main(String[] args) throws IOException {
        FileInputStream ins = new FileInputStream("C:\\Users\\User\\IdeaProjects\\" +
                "HomeWorks\\src\\main\\java\\ru\\khalitov\\lesson7\\VendAuto\\logging.properties");
        LogManager.getLogManager().readConfiguration(ins);

        VendAuto vendAuto = new VendAuto();

        vendAuto.getMenu();
        vendAuto.putMoney(35);
        vendAuto.getDrink(2);


    }
}

