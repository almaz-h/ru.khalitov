/*
 * Имя класса Task3_CountObject. Счетчик создаваемых объектов.
 *
 * Версия один, Урок 8ый ДЗ_8_3
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson8;

public class Task3_CountObject {
    private static int count;

    private Task3_CountObject() {
        count++;
    }

    private int getCount() {
        return count;
    }

    public static void main(String[] args) {
        Task3_CountObject Object1 = new Task3_CountObject();

        System.out.println(Object1.getCount());
        System.out.println(new Task3_CountObject().getCount());
        System.out.println(new Task3_CountObject().getCount());
    }
}

