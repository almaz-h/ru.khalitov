package ru.khalitov.lesson13;

class Son {

    static void eat(Food food) throws Exception {
        try {
            switch (food) {
                case SOUP:
                    throw new Exception();
                case CARROT:
                case APPLE:
                case PORRIDGE:
                    System.out.println("съел … за обе щеки");
            }
        } finally {
            System.out.println("Спасибо мама");
        }
    }
}

