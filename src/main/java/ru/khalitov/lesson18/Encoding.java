package ru.khalitov.lesson18;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class Encoding {
    public static void main(String[] args) {
        String win_cod = "src\\main\\java\\ru\\khalitov\\lesson18\\win_cod.txt";
        String utf_cod = "src\\main\\java\\ru\\khalitov\\lesson18\\utf_cod.txt";
        try {
            Files.write(Paths.get(win_cod),
                    new String(Files.readAllBytes(Paths.get(utf_cod)), Charset.forName("Windows-1251")).getBytes(StandardCharsets.UTF_8), StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
