/*
 * Имя класса Tas1_min_max. Поиск минимального числа из 2ух
 *
 * Урок 4ый, ДЗ4_1
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson4;

import java.util.Scanner;


public class Task1_min_max {
    public static void main(String[] args) {
        int a;
        int b;
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите первое число");
        a = sc.nextInt();
        System.out.println("Введите второе число");
        b = sc.nextInt();
        if (a < b) {
            System.out.printf("Число %d минимальное", a);
        } else if (a > b) {
            System.out.printf("Число %d минимальное", b);
        } else {
            System.out.println("Числа равны");
        }
    }
}


