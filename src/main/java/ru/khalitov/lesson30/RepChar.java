package ru.khalitov.lesson30;

import java.util.HashMap;
import java.util.Map;

class RepChar {
    public static void main(String[] args) {
        String line = "total";
        String line2 = "teter";

        findChar(line);
    }

    private static void findChar(String l) {
        Map<Character, Integer> charMap = new HashMap<>();

        for (int i = 0; i < l.length(); i++) {
            Character ch = l.charAt(i);
            if (null != charMap.get(ch)) {
                charMap.put(ch, charMap.get(ch) + 1);
            } else
                charMap.put(ch, 1);
        }
        for (int i = 0; i < l.length(); i++) {
            Character ch = l.charAt(i);
            int count = charMap.get(ch);
            if (count == 1) {
                System.out.println("Первый не повторяющийся символ: " + ch);
                break;
            }
        }
    }
}
