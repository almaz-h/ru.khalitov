package ru.khalitov.lesson6.OblastConcert;

class RapSinger extends Singer {
    RapSinger(String name, int age) {
        super(name, age);
    }

    void dance() {
        System.out.println("танцует пока поёт\n");
    }

    @Override
    void sing() {
        System.out.println(toString() + " и я читаю только рэп, ага, туц, руки-руки выше!");
    }

    @Override
    public String toString() {
        return super.toString();
    }
}