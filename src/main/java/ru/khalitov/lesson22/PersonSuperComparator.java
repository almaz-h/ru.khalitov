package ru.khalitov.lesson22;

import java.util.Comparator;

public class PersonSuperComparator {
    Comparator<Person> nameComparator = new Comparator<Person>() {

        public int compare(Person o1, Person o2) {
            int sName = o1.getName().compareTo(o2.getName());
            return sName;
        }
    };

    Comparator<Person> ageComparator = new Comparator<Person>() {
        public int compare(Person o1, Person o2) {
            return o1.getAge() - o2.getAge();
        }
    };
}

