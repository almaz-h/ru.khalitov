package ru.khalitov.lesson29;

import java.util.Objects;

public class Person {
    private String name;
    private String lastName;
    private Integer age;

    public String getName() {
        return name;
    }

    public Person(String name, String lastName, Integer age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(age, person.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lastName, age);
    }

    @Override
    public String toString() {
        return "Имя: " + name +
                ", Фамилия: " + lastName +
                ", возраст: " + age + "\n";
    }
}
