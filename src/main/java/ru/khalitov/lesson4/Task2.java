/*
 * Имя класса Task2. Описание введенного числа
 *
 * Урок 4ый, Д34_2
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson4;

import ru.khalitov.tools.ConsoleScanner;

public class Task2 {
    public static void main(String[] args) {
        int chislo;
        chislo = ConsoleScanner.NextFromConsole(); // предполагается ввод числе от одного до 100
        if (chislo > 0 && chislo < 10) {
            System.out.println(chislo % 2 == 0 ? "Четное однозначное число" : "Нечетное однозначное число");
        } else if (chislo > 10 && chislo < 100) {
            System.out.println(chislo % 2 == 0 ? "Четное двухзначное число" : "Нечетное двухзначное число");

        } else {
            System.out.println("Неизвестное значение");
        }
    }
}

