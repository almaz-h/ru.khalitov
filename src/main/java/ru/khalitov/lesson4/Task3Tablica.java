/*
 * Имя класса Task3. таблица умножения
 *
 * Урок 4ый, Д34_3
 *
 * Выполнил Алмаз Халитов
 */

package ru.khalitov.lesson4;

public class Task3Tablica {
    public static void main(String[] args) {
        int i;
        int j;
        for (i = 1; i < 10; i++) {
            for (j = 1; j < 10; j++) {
                System.out.printf("%d\t", i * j);
            }
            System.out.println();
        }
    }
}