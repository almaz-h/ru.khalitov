/*
 * Имя класса Fibonachi. Серия Фибонначи(два способа)
 *
 * Версия один. Урок 25 ДЗ_25_1
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson27;

public class Fibonachi {
    public static void main(String[] args) {
        printRecFib();

        System.out.println("Вызов печати серии Фибоначчи через итерационный цикл: ");
        iterFib(13);
    }

    private static void iterFib(int num) {
        int a = 0, b = 1, i, c;
        for (i = 0; i <= num; i++) {
            if (i <= 1) {
                c = i;
            } else {
                c = a + b;
                a = b;
                b = c;
            }
            System.out.print(c + ", ");
        }
    }

    private static void printRecFib() {
        System.out.println("Вызов печати серии Фибоначчи через рекурсивную функцию:");
        for (int i = 0; i <= 13; i++) {
            System.out.print(recFib(i) + ", ");
        }
        System.out.println();
    }

    private static int recFib(int n) {
        if (n == 0 || n == 1) {
            return n;
        } else {
            return recFib(n - 1) + recFib(n - 2);
        }
    }
}
