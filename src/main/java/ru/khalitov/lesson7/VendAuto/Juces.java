package ru.khalitov.lesson7.VendAuto;

enum Juces {
    COCACOLA("Кока-Кола", 40), PEPSI("Пепси", 35), SPRITE("Спрайт", 38);

    private String name;
    private int price;

    Juces(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }
}