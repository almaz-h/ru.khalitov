/*
 * Имя класса Lot. Сортировка множества
 *
 * Версия один. Урок 24 ДЗ_24
 *
 * Выполнил Алмаз Халитов
 */
package ru.khalitov.lesson24;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class Lot {
    public static void main(String[] args) {

        Set<String> set = createSet();

        printSet("Исходное множество: ", set);
        removeEvenLength(set);
        printSet("Множество после удаления элементов: ", set);
    }

    private static Set<String> createSet() {
        Set<String> set = new LinkedHashSet<>();
        set.add("foo");
        set.add("buzz");
        set.add("bar");
        set.add("fork");
        set.add("bort");
        set.add("spoon");
        set.add("!");
        set.add("dude");
        return set;
    }

    private static <S> void printSet(String title, Set<S> set) {
        System.out.println(title);
        for (S s : set) {
            System.out.println(s);
        }
        System.out.println();
    }

    private static Set<String> removeEvenLength(Set<String> set) {

        Iterator i = set.iterator();
        for (int counter = 0; counter < set.size(); counter++) {
            i.next();
            if (counter % 2 != 0) {
                i.remove();
            }
        }
        return set;
    }
}
